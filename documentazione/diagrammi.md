# Diagrammi UML

## Casi d'uso

Il programma recupera i dati delle previsioni meteorologiche da servizi internet, li salva in un database.
Attraverso uno script R è possibile analizzare le previsioni.

```plantuml
@startuml
left to right direction
actor "Utente" as Utente
rectangle {
  usecase "Scarica le previsioni meteorologiche" as UC1
  usecase "Salva le previsioni meteorologiche in un database" as UC2
  usecase "Analizza le previsioni meteorologiche" as UC3
}
Utente --> UC1
Utente --> UC2
Utente --> UC3
@enduml
```

## Diagrammi di attività

### Funzione principale

Dato un elenco di località, un servizio meteorologico e una connessione al database, itera sulle località e per ognuna ottieni le previsioni meteorologiche e salvale nel database. 

```plantuml
@startuml
start
:Ricerca database;
if (trovato?) then (no)
stop
else (sì)
:Importa Elenco Località;
:Creazione ServizioMeteo;
:Connessione al database;
while (Prossima località)
  :Ottieni previsione;
  :Salva previsione nel database;
endwhile (Fine)
  stop
@enduml
```

### Ottenimento previsione meteo

Il servizio meteo chiama l'API, ottenuti i dati in formato json, questi vengono estratti dalla classe Meteo.

```plantuml
@startuml
start
:Chiamata http all'API;
:Creazione del meteo;
:Estrazione dei dati dal formato json;
stop
@enduml
```

### Salvataggio previsioni nel database

La classe Meteo genera le righe da inserire nel database. La tabella genera la dichiarazione SQL per l'inserimento nel database.
L'esecuzione della query SQL avviene con il commit automatico disabilitato per migliorare le prestazioni del database.

```plantuml
@startuml
start
:Generazione delle righe per inserimento nel database;
:Generazione della dichiarazione SQL per inserimento;
:Disabilitazione commit automatico;
if (errore) then (sì)
  :Annullamento modifiche;
  :lancio eccezione;
  stop
else (no)
  :Commit modifiche;
  :Riabilitazione commit automatico;
  stop
@enduml
```

## Diagrammi delle classi

### ServizioMeteo

Interfaccia per i servizi meteorologici disponibili su internet. Ogni servizio deve restituire una class contenente i dati meteorologici e fornire la possibilità di testare il servizio online.
Sono disponibili due backend http statici, uno per il codice di produzione e uno per i test.

```plantuml
@startuml
interface ServizioMeteo {
{abstract} {method} Meteo ottieniPrevisioni(Località)
{abstract} {method} Risposta testConnessione(Località)
}
class ServizioMeteoAeronautica {
}
class ServizioMeteoSvizzera {
}

ServizioMeteo <|.. ServizioMeteoAeronautica
ServizioMeteo <|.. ServizioMeteoSvizzera
@enduml
```

### Meteo

Interfaccia per le classi che contengono i dati meteorologici ottenuti dai servizi meteorologici. Deve generare le previsioni meteorologiche (con una loro rappresentazione testuale) e generare le righe per inserire i dati meteorologici nel database.

```plantuml
@startuml
interface Meteo {
{abstract} {field} String fusoOrario
{abstract} {method} Vector[Previsione] estraiPrevisione(Long)
{abstract} {method} Vector[Previsione] estraiPrevisione()
{abstract} {method} Vector[MeteoRiga] generaRighePerDB
{method} Vector[String] testoPrevisione(Long)
{method} Unit stampaPrevisione(Long)
}
class MeteoSvizzera {}
class MeteoAeronautica {}

Meteo o-- Previsione
Meteo o-- MeteoRiga
Meteo <|.. MeteoSvizzera
Meteo <|.. MeteoAeronautica
MeteoAeronautica *-- Dataset
MeteoAeronautica *-- ExtraInfo
MeteoAeronautica *-- Modelli
MeteoAeronautica *-- Statistica
MeteoAeronautica o-- Coordinate
@enduml
```

### Tabella

Interfaccia per le tabelle nel database, la classe deve generare le dichiarazioni SQL per creare la tabella e per enumerare tutte le righe della tabella.

```plantuml
@startuml
interface Tabella {
{abstract} {field} String NomeTabella
{abstract} {method} String dichiarazioneCreaTabella
{method} String dichiarazioneSelezionaTabella
}
class MeteoTabella {
{method} PreparedStatement generaInserimento(Connection, Seq[MeteoRiga])
}
class MeteoRiga {
}
class Colonne {
{static} {field} String DATA_INSERIMENTO 
{static} {field} String DATA_PREVISIONE
{static} {field} String TEMPERATURA
{static} {field} String PRESSIONE
{static} {field} String VELOCITÀ_VENTO
{static} {field} String DIREZIONE_VENTO
{static} {field} String PIOGGIA_PROBABILITÀ
{static} {field} String FUSO_ORARIO
{static} {field} String LUOGO
{static} {field} String COORDINATE
}

Tabella <|.. MeteoTabella
MeteoTabella *-- MeteoRiga
MeteoTabella *-- Colonne
@enduml
```

## Configurazione località

Le località per cui scaricare le previsioni meteorologiche sono elencate nel file `configurazione.json`. Il file è in formato `json` ed è composto da una lista di località con la seguente struttura: 

```yaml
località:
  nome: string,
  latitudine: float
  longitudine: float
  servizioMeteo: string
  idSvizzera?: int
  attivo: boolean
  descrizione: string
```

Le coordinate due numeri in virgola mobile indicanti latitudine e longitudine.
Il `servizioMeteo` è un testo indicante per quale servizio meteorologico è necessario scaricare la previsione per quella località. La località deve essere ripetuta se deve essere scaricata da più servizi meteo. 

```
servizioMeteo: "aeronautica" | "svizzera"
```

## Versione PlantUML
```plantuml
@startuml
version
@enduml
```
