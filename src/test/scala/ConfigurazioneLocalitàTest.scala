package org.daingun.meteo

import analizzatori.AnalizzatoreUjson
import componenti.Località
import configurazione.ConfigurazioneLocalità

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers.{have, not}
import org.scalatest.matchers.should.Matchers.should
import org.scalatest.{BeforeAndAfterAll, Suite}

import java.nio.file.Path

trait Configurazione extends BeforeAndAfterAll {
  this: Suite =>
  /**
   * Importa la configurazione delle località.
   */
  val configLocalità: ConfigurazioneLocalità = ConfigurazioneLocalità(Path.of("src/test/scala/configurazione_test.json"), AnalizzatoreUjson())
  val listaLocalità: Vector[Località] = configLocalità.importaConfigurazione()
  print(listaLocalità)
}

class ConfigurazioneLocalitàTest extends AnyFlatSpec with Configurazione {
  "configurazione" should "funzionare" in {
    listaLocalità should not be null
    listaLocalità should have length 3
  }

  it should "avere due località per il meteo svizzera" in {
    listaLocalità.filter(_.servizioMeteo.contains("svizzera")) should have length 2
  }

  it should "avere una località per il meteo aeronautica" in {
    listaLocalità.filter(_.servizioMeteo.contains("aeronautica")) should have length 1
  }
}
