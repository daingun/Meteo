package org.daingun.meteo

import analizzatori.AnalizzatoreUjson
import clientiHttp.ClienteSttp.ClienteSttpTest
import componenti.Località
import configurazione.ConfigurazioneLocalità
import svizzera.ServizioMeteoSvizzera

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import org.scalatest.{BeforeAndAfterAll, Suite}

import java.io.{BufferedReader, FileReader}
import java.nio.file.Path
import scala.util.Using

trait DatiServizioMeteoSvizzera extends BeforeAndAfterAll {
  this: Suite =>
  /**
   * Dati meteo di test in formato json.
   */
  val versioniTest: String = Using.resource(BufferedReader(FileReader("src/test/scala/dati_test_svizzera_versioni.json"))) {
      lettore => Iterator.continually(lettore.readLine()).takeWhile(_ != null).toSeq
    }
    .mkString("")
  val datiTest: String = Using.resource(BufferedReader(FileReader("src/test/scala/dati_test_svizzera_valori.json"))) {
      lettore => Iterator.continually(lettore.readLine()).takeWhile(_ != null).toSeq
    }
    .mkString("")

  /**
   * Analizzatore dei dati.
   */
  val analizzatore: Analizzatore = AnalizzatoreUjson()

  /**
   * Località di test.
   */
  val lugano: Località = ConfigurazioneLocalità(Path.of("src/test/scala/configurazione_test.json"), analizzatore).importaConfigurazione().find(_.nome == "Lugano").get
}

trait SitoSvizzera extends BeforeAndAfterAll with DatiServizioMeteoSvizzera {
  this: Suite =>
  /**
   * URL per testare il recupero delle versioni delle previsioni.
   */
  val versioniUrl = "https://www.meteosvizzera.admin.ch/product/output/versions.json"
  /**
   * URL per testare la chiamata al servizio meteo con località LUGANO.
   */
  val testUrl = "https://www.meteosvizzera.admin.ch/product/output/forecast-chart/version__20231125_2045/it/690000.json"
  /**
   * Gestore delle connessioni con il comportamento necessario per il test.
   * Se riceve una chiamata HEAD da una risposta affermativa.
   * Se riceve una chiamata GET restituisce i dati meteo in formato json.
   */
  val cliente: ClienteHttp = ClienteSttpTest(testUrl, Array((versioniUrl, versioniTest), (testUrl, datiTest)))
}

class ServizioMeteoSvizzeraTest extends AnyFlatSpec with should.Matchers with DatiServizioMeteoSvizzera with SitoSvizzera {
  "servizio meteo svizzera" should "connettersi al sito" in {
    val ma = ServizioMeteoSvizzera(analizzatore, cliente)
    val risposta = ma.testConnessione(lugano)

    risposta.successo should be(true)
    risposta.corpo should be("Richiesta corretta")
  }

  it should "scaricare i dati meteorologici" in {
    val ma = ServizioMeteoSvizzera(analizzatore, cliente)
    val risposta = ma.ottieniPrevisione(lugano)
    println(risposta)

    risposta.toString should not be empty
  }
}
