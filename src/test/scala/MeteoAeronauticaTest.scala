package org.daingun.meteo

import aeronautica.MeteoAeronautica
import analizzatori.AnalizzatoreUjson
import pianoDati.{CreatoreSorgenteDati, Database}

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.*
import org.scalatest.{BeforeAndAfterAll, Suite}

import java.io.{BufferedReader, FileReader}
import scala.util.Using

trait Dati extends BeforeAndAfterAll {
  this: Suite =>
  /**
   * Dati meteo di test in formato json con località ESINO_LARIO.
   */
  val datiTest: String = Using.resource(BufferedReader(FileReader("src/test/scala/dati_test_aeronautica.json"))) {
      lettore => Iterator.continually(lettore.readLine()).takeWhile(_ != null).toSeq
    }
    .mkString("")

  /**
   * Analizzatore dei dati.
   */
  val analizzatore: Analizzatore = AnalizzatoreUjson()
}

class MeteoAeronauticaTest extends AnyFlatSpec with should.Matchers with Dati {
  "dati di test" should "non essere vuoto" in {
    datiTest should not be empty
  }

  "meteo" should "contenere i dati" in {
    val meteo = MeteoAeronautica(datiTest, "Esino Lario", analizzatore)
    println(meteo)
    meteo.toString should not be empty
    meteo.marcheTemporali.length should equal(meteo.dati.temperatura.length)
  }

  it should "estrarre la previsione" in {
    val meteo = MeteoAeronautica(datiTest, "Esino Lario", analizzatore)
    meteo.stampaPrevisione(1)
    meteo.estraiPrevisione(1) should have length 101
  }

  "i dati meteo" should "avere la stessa quantità di elementi" in {
    val dati = MeteoAeronautica(datiTest, "Esino Lario", analizzatore).dati
    val t = dati.temperatura.length
    val u = dati.umidità.length
    val pp = dati.pioggiaPerc.length
    val dg = dati.direzioneVentoGradi.length
    val dt = dati.direzioneVentoTesto.length
    val vv = dati.velocitàVento.length
    val vk = dati.velocitàVentokmh.length
    val tf = dati._2tf.length
    val i = dati.icona.length
    dati.temperatura should not be empty
    t should equal(u)
    u should equal(pp)
    pp should equal(dg)
    dg should equal(dt)
    dt should equal(vv)
    vv should equal(vk)
    vk should equal(tf)
    tf should equal(i)
  }

  "connessione al database" should "salvare le previsioni" in {
    val meteo = MeteoAeronautica(datiTest, "Esino Lario", analizzatore)

    val url = "jdbc:sqlite::memory:"
    Using.resource(Database(CreatoreSorgenteDati.creaSorgenteDatiSqlite(url))) { db =>
      val conto = db.salvaPrevisione(meteo)
      conto should be(101)
      noException should be thrownBy db.stampaTabelle()
    }
  }
}