package org.daingun.meteo

import aeronautica.ServizioMeteoAeronautica
import analizzatori.AnalizzatoreUjson
import clientiHttp.ClienteSttp.ClienteSttpTest
import componenti.Località
import configurazione.ConfigurazioneLocalità

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import org.scalatest.{BeforeAndAfterAll, Suite}

import java.io.{BufferedReader, FileReader}
import java.nio.file.Path
import scala.util.Using

trait DatiServizioMeteoAeronautica extends BeforeAndAfterAll {
  this: Suite =>
  /**
   * Dati meteo di test in formato json.
   */
  val datiTest: String = Using.resource(BufferedReader(FileReader("src/test/scala/dati_test_aeronautica.json"))) {
      lettore => Iterator.continually(lettore.readLine()).takeWhile(_ != null).toSeq
    }
    .mkString("")

  /**
   * Analizzatore dei dati.
   */
  val analizzatore: Analizzatore = AnalizzatoreUjson()

  /**
   * Località di test.
   */
  val cainallo: Località = ConfigurazioneLocalità(Path.of("src/test/scala/configurazione_test.json"), analizzatore).importaConfigurazione().find(_.nome == "Cainallo").get
}

trait Sito extends BeforeAndAfterAll with DatiServizioMeteoAeronautica {
  this: Suite =>
  /**
   * URL per testare la chiamata al servizio meteo con località CAINALLO.
   */
  val testUrl = "https://api.meteoam.it/deda-meteograms/api/GetMeteogram/preset1/45.98714,9.36531"
  /**
   * Gestore delle connessioni con il comportamento necessario per il test.
   * Se riceve una chiamata HEAD da una risposta affermativa.
   * Se riceve una chiamata GET restituisce i dati meteo in formato json.
   */
  val cliente: ClienteHttp = ClienteSttpTest(testUrl, Array((testUrl, datiTest)))
}

class ServizioMeteoAeronauticaTest extends AnyFlatSpec with should.Matchers with DatiServizioMeteoAeronautica with Sito {
  "servizio meteo aeronautica" should "connettersi al sito" in {
    val ma = ServizioMeteoAeronautica(analizzatore, cliente)
    val risposta = ma.testConnessione(cainallo)

    risposta.successo should be(true)
    risposta.corpo should be("Richiesta corretta")
  }

  it should "scaricare i dati meteorologici" in {
    val ma = ServizioMeteoAeronautica(analizzatore, cliente)
    val risposta = ma.ottieniPrevisione(cainallo)
    println(risposta)

    risposta.toString should not be empty
  }
}
