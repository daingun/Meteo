package org.daingun.meteo

import analizzatori.AnalizzatoreUjson
import componenti.Località
import configurazione.ConfigurazioneLocalità
import pianoDati.{CreatoreSorgenteDati, Database}
import svizzera.MeteoSvizzera

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.*
import org.scalatest.{BeforeAndAfterAll, Suite}

import java.io.{BufferedReader, FileReader}
import java.nio.file.Path
import scala.util.Using

trait DatiSvizzera extends BeforeAndAfterAll {
  this: Suite =>
  /**
   * Dati meteo di test in formato json con località LUGANO.
   */
  val datiTest: String = Using.resource(BufferedReader(FileReader("src/test/scala/dati_test_svizzera_valori.json"))) {
      lettore => Iterator.continually(lettore.readLine()).takeWhile(_ != null).toSeq
    }
    .mkString("")

  /**
   * Analizzatore dei dati.
   */
  val analizzatore: Analizzatore = AnalizzatoreUjson()

  val lugano: Località = ConfigurazioneLocalità(Path.of("src/test/scala/configurazione_test.json"), analizzatore).importaConfigurazione().find(_.nome == "Lugano").get
}

class MeteoSvizzeraTest extends AnyFlatSpec with should.Matchers with DatiSvizzera {
  "dati di test" should "non essere vuoto" in {
    datiTest should not be empty
  }

  "meteo" should "contenere i dati" in {
    val meteo = MeteoSvizzera(datiTest, lugano, analizzatore)
    println(meteo)
    meteo.toString should not be empty
    meteo.n_giorni should equal(9)
  }

  it should "estrarre la previsione" in {
    val meteo = MeteoSvizzera(datiTest, lugano, analizzatore)
    meteo.stampaPrevisione(1)
    meteo.estraiPrevisione(10) should have length (24 /*ore*/ * 9 /*giorni*/)
  }

  "i dati meteo" should "avere la stessa quantità di elementi" in {
    val dati = MeteoSvizzera(datiTest, lugano, analizzatore)
    val t = dati.temperatura.size
    val p = dati.pioggia.size
    val v = dati.ventoVel.size
    dati.temperatura should not be empty
    t should equal(p)
    p should equal(v)
  }

  "connessione al database" should "salvare le previsioni" in {
    val meteo = MeteoSvizzera(datiTest, lugano, analizzatore)

    val url = "jdbc:sqlite::memory:"
    Using.resource(Database(CreatoreSorgenteDati.creaSorgenteDatiSqlite(url))) { db =>
      val conto = db.salvaPrevisione(meteo)
      conto should be(24 /*ore*/ * 9 /*giorni*/)
      noException should be thrownBy db.stampaTabelle()
    }
  }
}