package org.daingun.meteo

import analizzatori.AnalizzatoreUjson

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import org.scalatest.{BeforeAndAfterAll, Suite}

import java.io.{BufferedReader, FileReader}
import scala.util.Using

trait DatiJson extends BeforeAndAfterAll {
  this: Suite =>
  /**
   * Dati meteo di test in formato json con località ESINO_LARIO.
   */
  val datiTest: String = Using.resource(BufferedReader(FileReader("src/test/scala/dati_test_aeronautica.json"))) {
      lettore => Iterator.continually(lettore.readLine()).takeWhile(_ != null).toSeq
    }
    .mkString("")
}

class AnalizzatoreUjsonTest extends AnyFlatSpec with should.Matchers with Dati {
  "testoEstratto" should "contenere un vettore di stringhe" in {
    val auj = AnalizzatoreUjson()
    auj.leggi(datiTest)
    val risultato = auj.estraiListaTesto("paramlist")
    risultato should have length 10
  }

  it should "contenere un vettore di numeri" in {
    val auj = AnalizzatoreUjson()
    auj.leggi(datiTest)
    val risultato = auj.estraiListaNumeri("pointlist", 0)
    risultato should have length 2
  }

  it should "contenere un dizionario di stringhe" in {
    val auj = AnalizzatoreUjson()
    auj.leggi(datiTest)
    val risultato = auj.estraiDizionarioTesto("datasets", "0", "5")
    risultato should have size 101
  }

  it should "contenere un dizionario di numeri" in {
    val auj = AnalizzatoreUjson()
    auj.leggi(datiTest)
    val risultato = auj.estraiDizionarioNumeri("datasets", "0", "2")
    risultato should have size 101
  }

  it should "contenere un testo" in {
    val auj = AnalizzatoreUjson()
    auj.leggi(datiTest)
    val risultato = auj.estraiTesto("extrainfo", "stats", 0, "icon")
    risultato should equal("04")
  }

  it should "contenere un numero" in {
    val auj = AnalizzatoreUjson()
    auj.leggi(datiTest)
    val risultato = auj.estraiNumero("extrainfo", "elapsed")
    risultato should equal(1.0767908096313477)
  }

  it should "contenere una lista al livello più alto" in {
    val auj = AnalizzatoreUjson()
    auj.leggi("[ 1, 2, 3, 4 ]")
    val risultato = auj.estraiListaNumeri()
    risultato should equal(Vector(1.0, 2.0, 3.0, 4.0))
  }
}
