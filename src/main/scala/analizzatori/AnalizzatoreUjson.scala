package org.daingun.meteo
package analizzatori

import ujson.Value
import ujson.Value.Selector

import scala.collection.Map
import scala.collection.mutable.ArrayBuffer

/**
 * Classe che realizza un analizzatore json usando la libreria ujson.
 */
class AnalizzatoreUjson extends Analizzatore {
  /**
   * Oggetto contenente la rappresentazione dei dati in formato json.
   */
  private var json: Value = _

  override def leggi(testo: String): Unit = {
    json = ujson.read(testo)
  }

  override def estraiListaTesto(selettore: Int | String*): Vector[String] = {
    val risultato: Value = estrai(selettore)
    listaDiTesti(risultato.arr)
  }

  private def listaDiTesti(dataset: ArrayBuffer[Value]) =
    dataset.map(v => v.str).toVector

  override def estraiDizionarioTesto(selettore: Int | String*): Map[String, String] = {
    val risultato: Value = estrai(selettore)
    DizionarioDiTesti(risultato.obj)
  }

  private def DizionarioDiTesti(dataset: Map[String, Value]) =
    dataset.map((k, v) => (k, v.strOpt.getOrElse(v.toString))).toMap

  override def estraiDizionarioNumeri(selettore: Int | String*): Map[String, Double] = {
    val risultato: Value = estrai(selettore)
    DizionarioDiNumeri(risultato.obj)
  }

  private def DizionarioDiNumeri(dataset: Map[String, Value]) =
    dataset.map((k, v) => (k, v.numero)).toMap

  override def estraiListaNumeri(selettore: Int | String*): Vector[Double] = {
    val risultato: Value = estrai(selettore)
    listaDiNumeri(risultato.arr)
  }

  private def listaDiNumeri(dataset: ArrayBuffer[Value]) =
    dataset.map(v => v.numero).toVector

  override def estraiTesto(selettore: Int | String*): String = {
    estrai(selettore).str
  }

  /**
   * Metodo per estrarre un elemento json data una collezione di selettori.
   *
   * @param selettore Collezione di selettori json.
   * @return Elemento selezionato.
   */
  private def estrai(selettore: Seq[Int | String]): Value = {
    selettore.foldLeft(json)((j, x) => {
      x match
        case i: Int => j(i)
        case s: String => j(s)
    })
  }

  override def estraiNumero(selettore: Int | String*): Double = {
    estrai(selettore).numero
  }

  override def estraiListaDizionariTesto(selettore: Int | String*): Vector[Map[String, String]] = {
    val risultato: Value = estrai(selettore)
    risultato.arr.map(x => DizionarioDiTesti(x.obj)).toVector
  }

  override def estraiLunghezzaLista(selettore: Int | String*): Int = {
    val risultato: Value = estrai(selettore)
    risultato.arr.length
  }

  extension (n: Value) {
    /**
     * Converti un Valore json in numero
     *
     * @return Numero o NAN se non è possibile la conversione
     */
    def numero: Double = {
      try
        n.num
      catch
        case e: Exception => Double.NaN
    }
  }
}
