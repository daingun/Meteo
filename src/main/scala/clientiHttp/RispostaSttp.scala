package org.daingun.meteo
package clientiHttp

import sttp.client4.Response

class RispostaSttp(risposta: Response[String]) extends Risposta {

  override def corpo: String = risposta.body

  override def successo: Boolean = risposta.code.isSuccess
}
