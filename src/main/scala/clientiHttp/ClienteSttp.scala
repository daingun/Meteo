package org.daingun.meteo
package clientiHttp

import sttp.client4.httpclient.HttpClientSyncBackend
import sttp.client4.testing.SyncBackendStub
import sttp.client4.{SyncBackend, UriContext, quickRequest}
import sttp.model.Method

/**
 * Cliente http che usa la libreria sttp.
 *
 * @param backend Gestore delle connessioni per le richieste http.
 */
class ClienteSttp(backend: SyncBackend) extends ClienteHttp {

  override def richiestaGet(uri: String): Risposta = {
    RispostaSttp(
      quickRequest
        .get(uri"$uri")
        .send(backend))
  }

  override def richiestaHead(uri: String): Risposta = {
    RispostaSttp(
      quickRequest
        .head(uri"$uri")
        .send(backend))
  }
}

/**
 * Oggetto compagno della classe ClienteHttp.
 */
object ClienteSttp {
  /**
   * Gestore delle connessioni http per i test.
   */
  private val BACKEND_TEST: SyncBackendStub = SyncBackendStub

  /**
   * Gestore delle connessioni http per la produzione.
   */
  private val BACKEND_PRODUZIONE: SyncBackend = HttpClientSyncBackend()

  /**
   * Cliente http per la produzione.
   *
   * @return Cliente http.
   */
  def ClienteSttpProduzione: ClienteHttp = ClienteSttp(BACKEND_PRODUZIONE)

  /**
   * Cliente http per i test.
   *
   * @param testUrlHead Url per i test della connessione.
   * @param url_dati    Collezione di tuple contenenti l'url e la relativa risposta che deve essere restituita.
   * @return Cliente http.
   */
  def ClienteSttpTest(testUrlHead: String, url_dati: Seq[(String, String)]): ClienteHttp = {
    var backend = BACKEND_TEST
      .whenRequestMatches(r => r.method == Method.HEAD && r.uri.toString == testUrlHead)
      .thenRespond("Richiesta corretta")
    for ((testUrl, datiTest) <- url_dati) {
      backend = backend
        .whenRequestMatches(r => r.method == Method.GET && r.uri.toString == testUrl)
        .thenRespond(datiTest)
    }
    backend.whenAnyRequest
      .thenRespondServerError()
    ClienteSttp(backend)
  }
}


