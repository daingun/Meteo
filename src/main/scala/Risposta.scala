package org.daingun.meteo

/**
 * Risposta http.
 */
trait Risposta {
  /**
   * Corpo della risposta.
   *
   * @return Corpo della risposta come stringa.
   */
  def corpo: String

  /**
   * Determina se la richiesta ha avuto successo.
   *
   * @return Vero se la richiesta ha avuto successo.
   */
  def successo: Boolean
}
