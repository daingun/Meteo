package org.daingun.meteo
package configurazione

import Estensioni.numero
import componenti.{Coordinate, Località}

import java.io.{BufferedReader, FileReader}
import java.nio.file.Path
import scala.collection.Map
import scala.util.Using

/**
 * Classe per l'importazione del file di configurazione contenente la lista di località.
 *
 * @param percorso Percorso al file di configurazione
 */
class ConfigurazioneLocalità(percorso: Path, analizzatore: Analizzatore) {
  /**
   * Genera la lista di località presente nella configurazione.
   *
   * @return Vettore contenente la lista di località
   */
  def importaConfigurazione(): Vector[Località] = {
    val datiConfigurazione: String = Using.resource(BufferedReader(FileReader(percorso.toFile))) {
        lettore => Iterator.continually(lettore.readLine()).takeWhile(_ != null).toSeq
      }
      .mkString("")
    analizzatore.leggi(datiConfigurazione)
    val configurazione = analizzatore.estraiListaDizionariTesto()
    configurazione.map(l => creaLocalità(l))

  }

  /**
   * Crea un oggetto `Località` dalla configurazione in formato json.
   *
   * @param valore Dizionario contenente i dati della località
   * @return Oggetto di tipo `Località`
   */
  private def creaLocalità(valore: Map[String, String]): Località = {
    val nome = valore("nome")
    val latitudine = valore("latitudine").numero
    val longitudine = valore("longitudine").numero
    val servizioMeteo = valore("servizioMeteo")
    val attivo = valore("attivo").toBoolean
    val descrizione = valore("descrizione")
    // La presenza di `idSvizzera` è opzionale.
    val idSvizzera = valore.get("idSvizzera").map(_.toInt).getOrElse(0)

    Località(nome, Coordinate(latitudine, longitudine), idSvizzera, servizioMeteo, attivo, descrizione)
  }
}
