package org.daingun.meteo

import java.time.format.DateTimeFormatter
import java.time.{Instant, LocalDateTime}

object Estensioni {
  extension (tempo: String) {
    /**
     * Converti una marca temporale da testo in LocalDateTime
     *
     * @return Data e ora locali
     */
    def tempoDaTesto: LocalDateTime = LocalDateTime.parse(tempo, DateTimeFormatter.ISO_OFFSET_DATE_TIME)

    /**
     * Converti una marca temporale testuale da UTC al fuso orario indicato.
     *
     * @return Data e ora locali
     */
    def istanteDaTestoUTC: Instant = Instant.parse(tempo)
  }

  extension (puntoCardinale: String) {
    /**
     * Traduci ovest in italiano (W -> O)
     *
     * @return Punto cardinale in italiano
     */
    def traduciOvest: String = puntoCardinale.replace("W", "O")
  }

  extension (n: String) {
    /**
     * Converti una stringa in numero
     *
     * @return Numero o NAN se non è possibile la conversione
     */
    def numero: Double = {
      n.toDoubleOption.getOrElse(Double.NaN)
    }
  }
}
