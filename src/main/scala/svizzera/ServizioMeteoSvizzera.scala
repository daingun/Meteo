package org.daingun.meteo
package svizzera

import componenti.Località

/**
 * Servizio meteorologico della Svizzera.
 *
 * @param cliente Cliente http.
 */
class ServizioMeteoSvizzera(analizzatore: Analizzatore, cliente: ClienteHttp) extends ServizioMeteo {
  /**
   * Url per ottenere le versioni delle previsioni meteorologiche.
   */
  val versioniUrl = "https://www.meteosvizzera.admin.ch/product/output/versions.json"

  override def ottieniPrevisione(località: Località): Meteo = {
    val versione: String = ottieniVersione
    val risposta = cliente.richiestaGet(urlBase(versione, località.idSvizzera))
    MeteoSvizzera(risposta.corpo, località, analizzatore)
  }

  override def testConnessione(località: Località): Risposta = {
    val versione: String = ottieniVersione
    cliente.richiestaHead(urlBase(versione, località.idSvizzera))
  }

  /**
   * Ottieni la versione più recente delle previsioni meteorologiche.
   *
   * @return Stringa di testo contenente la versione
   */
  private def ottieniVersione = {
    val versioni = cliente.richiestaGet(versioniUrl)
    analizzatore.leggi(versioni.corpo)
    analizzatore.estraiTesto("forecast-chart")
  }

  /**
   * Crea l'url per recuperare i dati delle previsioni.
   *
   * Esempio: `https://www.meteosvizzera.admin.ch/product/output/forecast-chart/version__20231125_2045/it/690000.json`
   *
   * @param versione   versione delle previsioni
   * @param idSvizzera identificativo della località
   * @return url per ottenere i dati
   */
  private def urlBase(versione: String, idSvizzera: Int): String = s"https://www.meteosvizzera.admin.ch/product/output/forecast-chart/version__$versione/it/$idSvizzera.json"
}
