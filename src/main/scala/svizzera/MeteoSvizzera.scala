package org.daingun.meteo
package svizzera

import Estensioni.traduciOvest
import componenti.{Località, Previsione}
import pianoDati.MeteoRiga

import java.time.Instant
import java.time.temporal.ChronoUnit
import scala.collection.immutable.TreeMap

/**
 * Classe per la generazione delle previsioni della Svizzera.
 *
 * @param datiMeteo Dati meteo in formato json
 * @param luogo     Luogo della previsione
 */
class MeteoSvizzera(datiMeteo: String, luogo: Località, analizzatore: Analizzatore) extends Meteo {
  /**
   * Dati meteo in formato json.
   */
  analizzatore.leggi(datiMeteo)
  /**
   * Numero di giorni presenti nei dati.
   */
  private[meteo] val n_giorni = analizzatore.estraiLunghezzaLista()
  /**
   * Temperature.
   */
  private[meteo] val temperatura: TreeMap[Long, Double] = {
    valoriNumerici("temperature")
  }
  /**
   * Quantità di pioggia.
   */
  private[meteo] val pioggia: TreeMap[Long, Double] = {
    valoriNumerici("rainfall")
  }
  /**
   * Velocità del vento.
   */
  private[meteo] val ventoVel: TreeMap[Long, Double] = {
    valoriNumerici2("wind", "data")
  }
  /**
   * Direzione del vento.
   */
  private val ventoDir: TreeMap[Long, String] = {
    valoriTesto2("wind", "symbols")
  }

  override def estraiPrevisione(giorni: Long): Vector[Previsione] = {
    estraiPrevisione()
      .filter(_.tempo.isBefore(Instant.now().plus(giorni, ChronoUnit.DAYS)))
  }

  override def estraiPrevisione(): Vector[Previsione] =
    temperatura.map(creaPrevisione).toVector

  /**
   * @param istante     Istante di tempo della previsione (formato unix)
   * @param temperatura Temperatura associata all'istante
   * @return Previsione meteorologica
   */
  private def creaPrevisione(istante: Long, temperatura: Double): Previsione =
    Previsione(
      tempo = Instant.ofEpochMilli(istante),
      temperatura = temperatura,
      pressione = Double.NaN,
      velocitàVentokmh = ventoVel.getOrElse(istante, Double.NaN),
      direzioneVentoTesto = ventoDir.getOrElse(istante, ""),
      pioggiaProb = pioggia.getOrElse(istante, Double.NaN)
    )

  override def generaRighePerDB: Vector[MeteoRiga] =
    val adesso = Instant.now().truncatedTo(ChronoUnit.SECONDS)
    estraiPrevisione().map(
      p => MeteoRiga(
        dataInserimento = adesso,
        dataPrevisione = p.tempo,
        temperatura = p.temperatura,
        pressione = p.pressione,
        velocitàVento = p.velocitàVentokmh,
        direzioneVento = p.direzioneVentoTesto,
        pioggiaProbabilità = p.pioggiaProb,
        fusoOrario = fusoOrario,
        luogo = luogo.nome,
        coordinate = luogo.coordinate.comeTesto
      )
    )

  override def fusoOrario: String = "Europe/Zurich"

  override def toString: String =
    val testo = StringBuilder()
    testo.append(temperatura)
    testo.append(pioggia)
    testo.append(ventoVel)
    testo.append(ventoDir)
    testo.toString()

  /**
   * Estrai i valori numerici nella data chiave.
   *
   * @param chiave Selettore json.
   * @return Dizionario di valori.
   */
  private def valoriNumerici(chiave: String): TreeMap[Long, Double] = {
    TreeMap.from(
      Range(0, n_giorni).flatMap(giorno => {
        Range(0, 24).map(ora => {
          val r = analizzatore.estraiListaNumeri(giorno, chiave, ora)
          (r(0).toLong, r(1))
        })
      })
    )
  }

  /**
   * Estrai i valori numerici date due chiavi.
   *
   * @param chiave1 Primo selettore json.
   * @param chiave2 Secondo selettore json.
   * @return Dizionario di valori.
   */
  private def valoriNumerici2(chiave1: String, chiave2: String): TreeMap[Long, Double] = {
    TreeMap.from(
      Range(0, n_giorni).flatMap(giorno => {
        Range(0, 24).map(ora => {
          val r = analizzatore.estraiListaNumeri(giorno, chiave1, chiave2, ora)
          (r(0).toLong, r(1))
        })
      })
    )
  }

  /**
   * Estrai i valori testuali date due chiavi.
   *
   * @param chiave1 Primo selettore json.
   * @param chiave2 Secondo selettore json.
   * @return Dizionario di valori.
   */
  private def valoriTesto2(chiave1: String, chiave2: String): TreeMap[Long, String] = {
    TreeMap.from(
      Range(0, n_giorni).flatMap(giorno => {
        Range(0, 12).map(ora => {
          val r = analizzatore.estraiDizionarioTesto(giorno, chiave1, chiave2, ora)
          (r("timestamp").toLong, r("symbol_id").traduciOvest)
        })
      })
    )
  }
}
