package org.daingun.meteo
package componenti

import java.time.format.DateTimeFormatter
import java.time.{Instant, LocalDateTime, ZoneId}

/**
 * Classe dati per la previsione meteorologica
 *
 * @param tempo               data e ora
 * @param temperatura         temperatura in Celsius
 * @param velocitàVentokmh    velocità del vento in km/h
 * @param pressione           pressione atmosferica in hPa
 * @param direzioneVentoTesto direzione testuale del vento
 * @param pioggiaProb         probabilità di pioggia in percentuale
 */
case class Previsione(tempo: Instant, temperatura: Double, pressione: Double, velocitàVentokmh: Double, direzioneVentoTesto: String, pioggiaProb: Double):
  /**
   * Stampa la previsione come testo.
   *
   * @param fusoOrario fuso orario della località della previsione
   * @return Rappresentazione testuale della previsione
   */
  def stampaPrevisione(fusoOrario: String): String =
    val ora = LocalDateTime.ofInstant(tempo, ZoneId.of(fusoOrario))
    val sb = StringBuilder()
    sb.append(s"${ora.format(DateTimeFormatter.ISO_LOCAL_DATE)}")
    sb.append(s"\t${ora.format(DateTimeFormatter.ofPattern("HH:mm"))}")
    sb.append(s"\t$temperatura")
    sb.append(s"\t$pressione")
    sb.append(f"\t$velocitàVentokmh%-5s")
    sb.append(f"\t$direzioneVentoTesto%-5s")
    sb.append(s"\t$pioggiaProb")
    sb.toString

/**
 * Oggetto compagno della classe Previsione.
 */
object Previsione:
  /**
   * Rappresentazione testuale dei campi stampati dal metodo stampaPrevisione
   */
  val INTESTAZIONE_PREVISIONE: String =
    "giorno\tora\ttemp.\tpress.\tvel.vento\tdir.vento\tprob.pioggia"

end Previsione
