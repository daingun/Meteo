package org.daingun.meteo
package componenti

/**
 * Località per le previsioni meteorologiche.
 *
 * @param nome          Nome della località
 * @param coordinate    Coordinate della località
 * @param idSvizzera    Identificativo meteo Svizzera
 * @param servizioMeteo Servizio meteo disponibile per la località
 * @param attivo        Vero se la previsione deve essere scaricata
 * @param descrizione   Descrizione della località
 */
case class Località(nome: String, coordinate: Coordinate, idSvizzera: Int, servizioMeteo: String, attivo: Boolean, descrizione: String)