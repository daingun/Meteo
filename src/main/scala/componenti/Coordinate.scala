package org.daingun.meteo
package componenti

/**
 * Coordinate del punto considerato
 *
 * @param latitudine  Latitudine della coordinata
 * @param longitudine Longitudine della coordinata
 */
case class Coordinate(latitudine: Double, longitudine: Double):
  /**
   * Rappresentazione testuale per delle coordinate per il database e le chiamate http.
   *
   * @return Rappresentazione testuale delle coordinate
   */
  def comeTesto: String =
    s"$latitudine,$longitudine"

end Coordinate

object Coordinate:
  /**
   * Coordinate del punto considerato
   *
   * @param coordinate Lista contenente latitudine e longitudine
   */
  def apply(coordinate: IndexedSeq[Double]): Coordinate =
    this (coordinate(0), coordinate(1))

end Coordinate
