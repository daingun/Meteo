package org.daingun.meteo
package aeronautica

/**
 * Informazioni extra
 *
 * @param tempoTrascorso Tempo trascorso
 * @param modelli        Modelli meteorologici
 * @param altitudine     Altitudine del luogo geografico
 * @param fusoOrario     Fuso orario
 * @param statistiche    Statistiche
 */
case class InformazioniExtra(tempoTrascorso: Double, modelli: Modelli,
                             altitudine: Double, fusoOrario: String,
                             statistiche: Vector[Statistica])

object InformazioniExtra:
  /**
   * Informazioni extra
   *
   * @param analizzatore Analizzatore con i dati.
   */
  def apply(analizzatore: Analizzatore): InformazioniExtra = {
    this (
      tempoTrascorso = analizzatore.estraiNumero("extrainfo", "elapsed"),
      modelli = Modelli(analizzatore.estraiDizionarioTesto("extrainfo", "models")),
      altitudine = analizzatore.estraiNumero("extrainfo", "height"),
      fusoOrario = analizzatore.estraiTesto("extrainfo", "timezone"),
      statistiche = analizzatore.estraiListaDizionariTesto("extrainfo", "stats").map(x => Statistica(x)))
  }
end InformazioniExtra
