package org.daingun.meteo
package aeronautica

import scala.collection.Map

/**
 * Modelli di previsione meteorologica
 *
 * @param cosmo_it Modello cosmo
 * @param ecmwf    Modello ecmwf
 */
case class Modelli(cosmo_it: String, ecmwf: String)

object Modelli:
  /**
   * Modelli di previsione meteorologica
   *
   * @param modelli Dizionario contenente i modelli
   */
  def apply(modelli: Map[String, String]): Modelli =
    this (modelli("cosmo_it"), modelli("ecmwf"))
end Modelli
