package org.daingun.meteo
package aeronautica

import Estensioni.istanteDaTestoUTC
import componenti.{Coordinate, Previsione}
import pianoDati.MeteoRiga

import java.time.Instant
import java.time.temporal.ChronoUnit

/**
 * Classe per la generazione delle previsioni dell'Aeronautica Militare.
 *
 * @param datiMeteo    Dati meteo in formato json.
 * @param luogo        Luogo della previsione.
 * @param analizzatore Analizzatore dei dati.
 */
class MeteoAeronautica(datiMeteo: String, luogo: String, analizzatore: Analizzatore) extends Meteo {
  /**
   * Dati meteo in formato json.
   */
  analizzatore.leggi(datiMeteo)
  /**
   * Informazioni extra sul luogo delle previsioni.
   */
  private val extraInfo = InformazioniExtra(analizzatore)
  /**
   * Marca temporale delle previsioni.
   */
  private[meteo] val marcheTemporali = analizzatore.estraiListaTesto("timeseries").map(_.istanteDaTestoUTC)
  /**
   * Coordinate del luogo delle previsioni meteo.
   */
  private val coordinate = Coordinate(analizzatore.estraiListaNumeri("pointlist", 0))
  /**
   * Nome dei parametri dei dati delle previsioni.
   */
  private val parametri = analizzatore.estraiListaTesto("paramlist")
  /**
   * Dati meteo delle previsioni
   */
  private[meteo] val dati = Dataset(analizzatore)

  override def generaRighePerDB: Vector[MeteoRiga] =
    val adesso = Instant.now().truncatedTo(ChronoUnit.SECONDS)
    estraiPrevisione().map(
      p => MeteoRiga(
        dataInserimento = adesso,
        dataPrevisione = p.tempo,
        temperatura = p.temperatura,
        pressione = p.pressione,
        velocitàVento = p.velocitàVentokmh,
        direzioneVento = p.direzioneVentoTesto,
        pioggiaProbabilità = p.pioggiaProb,
        fusoOrario = extraInfo.fusoOrario,
        luogo = luogo,
        coordinate = coordinate.comeTesto
      )
    )

  override def estraiPrevisione(): Vector[Previsione] =
    marcheTemporali
      .iterator
      .zipWithIndex
      .map(creaPrevisione)
      .toVector

  override def fusoOrario: String = extraInfo.fusoOrario

  override def estraiPrevisione(giorni: Long): Vector[Previsione] =
    marcheTemporali
      .iterator
      .zipWithIndex
      .filter((t, i) => t.isBefore(Instant.now().plus(giorni, ChronoUnit.DAYS)))
      .map(creaPrevisione)
      .toVector

  /**
   * Crea una previsione.
   *
   * @param istante marca temporale
   * @param i       indice della marca temporale
   * @return Previsione meteorologica
   */
  private def creaPrevisione(istante: Instant, i: Int): Previsione =
    Previsione(
      tempo = istante,
      temperatura = dati.temperatura(i),
      pressione = dati.pressione(i),
      velocitàVentokmh = dati.velocitàVentokmh(i),
      direzioneVentoTesto = dati.direzioneVentoTesto(i),
      pioggiaProb = dati.pioggiaPerc(i)
    )

  override def toString: String =
    val testo = StringBuilder()
    testo.append(marcheTemporali)
    testo.append(coordinate)
    testo.append(parametri)
    testo.append(extraInfo)
    testo.append(dati)
    testo.toString()

}