package org.daingun.meteo
package aeronautica

import Estensioni.traduciOvest

import scala.collection.Map

/**
 * Dati meteorologici
 * ArrayBuffer("2t", "r", "pmsl", "tpp", "wdir", "wcar", "wspd", "wkmh", "2tf", "icon")
 *
 * @param temperatura         2t celsius
 * @param umidità             r %
 * @param pressione           pmsl hPa
 * @param pioggiaPerc         tpp %
 * @param direzioneVentoGradi wdir gradi
 * @param direzioneVentoTesto wcar punti cardinali
 * @param velocitàVento       wspd
 * @param velocitàVentokmh    wkmh km/hh
 * @param _2tf                2tf
 * @param icona               icon
 */
case class Dataset(temperatura: Vector[Double],
                   umidità: Vector[Double],
                   pressione: Vector[Double],
                   pioggiaPerc: Vector[Double],
                   direzioneVentoGradi: Vector[Double],
                   direzioneVentoTesto: Vector[String],
                   velocitàVento: Vector[Double],
                   velocitàVentokmh: Vector[Double],
                   _2tf: Vector[Double],
                   icona: Vector[String]
                  )

object Dataset:
  /**
   * Dati meteorologici
   *
   * @param analizzatore Analizzatore contenente i dati
   */
  def apply(analizzatore: Analizzatore): Dataset =
    this (
      temperatura = daDizionarioALista(analizzatore.estraiDizionarioNumeri("datasets", "0", "0")),
      umidità = daDizionarioALista(analizzatore.estraiDizionarioNumeri("datasets", "0", "1")),
      pressione = daDizionarioALista(analizzatore.estraiDizionarioNumeri("datasets", "0", "2")),
      pioggiaPerc = daDizionarioALista(analizzatore.estraiDizionarioNumeri("datasets", "0", "3")),
      direzioneVentoGradi = daDizionarioALista(analizzatore.estraiDizionarioNumeri("datasets", "0", "4")),
      direzioneVentoTesto = daDizionarioALista(analizzatore.estraiDizionarioTesto("datasets", "0", "5")).map(_.traduciOvest),
      velocitàVento = daDizionarioALista(analizzatore.estraiDizionarioNumeri("datasets", "0", "6")),
      velocitàVentokmh = daDizionarioALista(analizzatore.estraiDizionarioNumeri("datasets", "0", "7")),
      _2tf = daDizionarioALista(analizzatore.estraiDizionarioNumeri("datasets", "0", "8")),
      icona = daDizionarioALista(analizzatore.estraiDizionarioTesto("datasets", "0", "9"))
    )

  /**
   * Converti un dizionario le cui chiavi sono stringhe di numeri in una lista, i cui elementi
   * sono i valori del dizionario posizionati all'indice definito dalla chiave.
   *
   * @param dizionario Dizionario con chiavi stringhe di numeri.
   * @tparam T Tipo dei valori del dizionario.
   * @return Lista.
   */
  private def daDizionarioALista[T](dizionario: Map[String, T]): Vector[T] = {
    val n = dizionario.size
    Vector.tabulate(n) { i => dizionario(i.toString) }
  }
end Dataset
