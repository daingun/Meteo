package org.daingun.meteo
package aeronautica

import Estensioni.{numero, tempoDaTesto}

import java.time.LocalDateTime
import scala.collection.Map

/**
 * Statistiche del luogo considerato
 *
 * @param dataLocale    Data e ora locali
 * @param maxCelsius    Massima temperatura in Celsius
 * @param minCelsius    Minima temperatura in Celsius
 * @param maxFahrenheit Massima temperatura in Fahrenheit
 * @param minFahrenheit Minima temperatura in Fahrenheit
 * @param icona         Icona
 */
case class Statistica(dataLocale: LocalDateTime, maxCelsius: Double,
                      minCelsius: Double, maxFahrenheit: Double,
                      minFahrenheit: Double, icona: String)

object Statistica:
  /**
   * Statistiche del luogo considerato
   *
   * @param dati Dizionario contenente i dati
   */
  def apply(dati: Map[String, String]): Statistica =
    this (dataLocale = dati("localDate").tempoDaTesto,
      maxCelsius = dati("maxCelsius").numero,
      minCelsius = dati("minCelsius").numero,
      maxFahrenheit = dati("maxFahrenheit").numero,
      minFahrenheit = dati("minFahrenheit").numero,
      icona = dati("icon")
    )
end Statistica
