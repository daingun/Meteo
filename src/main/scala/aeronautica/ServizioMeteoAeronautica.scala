package org.daingun.meteo
package aeronautica

import componenti.Località

/**
 * Servizio meteorologico dell'Aeronautica Militare.
 *
 * @param analizzatore Analizzatore dei dati.
 * @param cliente      Cliente http.
 */
class ServizioMeteoAeronautica(analizzatore: Analizzatore, cliente: ClienteHttp) extends ServizioMeteo {
  /**
   * Url base del servizio dell'Aeronautica Militare.
   */
  private val urlBase = "https://api.meteoam.it/deda-meteograms/api/GetMeteogram/preset1/"

  override def ottieniPrevisione(località: Località): MeteoAeronautica = {
    val risposta = cliente.richiestaGet(s"$urlBase${località.coordinate.comeTesto}")
    MeteoAeronautica(risposta.corpo, località.nome, analizzatore)
  }

  override def testConnessione(località: Località): Risposta = {
    cliente.richiestaHead(s"$urlBase${località.coordinate.comeTesto}")
  }
}
