package org.daingun.meteo

import aeronautica.ServizioMeteoAeronautica
import analizzatori.AnalizzatoreUjson
import clientiHttp.ClienteSttp.ClienteSttpProduzione
import componenti.Località
import configurazione.ConfigurazioneLocalità
import pianoDati.{CreatoreSorgenteDati, Database}
import svizzera.ServizioMeteoSvizzera

import java.nio.file.{Files, Path}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import scala.util.Using

@main
def main(): Unit = {
  val percorso: Path = Path.of("db/meteo.db").toAbsolutePath
  if !Files.exists(percorso) then
    println(s"Il database $percorso non esiste")
    return;

  val url = s"jdbc:sqlite:$percorso"
  println(s"Database: $url")

  val sorgenteDati = CreatoreSorgenteDati.creaSorgenteDatiSqlite(url)

  val configurazioneLocalità = ConfigurazioneLocalità(Path.of("configurazione.json"), AnalizzatoreUjson())
  val località: Vector[Località] = configurazioneLocalità.importaConfigurazione().filter(_.attivo)

  val luoghiAeronautica = località.filter(_.servizioMeteo.contains("aeronautica"))
  val luoghiSvizzera = località.filter(_.servizioMeteo.contains("svizzera"))

  Using.resource(Database(sorgenteDati)) { db =>
    scaricaPrevisioni(ServizioMeteoAeronautica(AnalizzatoreUjson(), ClienteSttpProduzione), luoghiAeronautica, db)
    scaricaPrevisioni(ServizioMeteoSvizzera(AnalizzatoreUjson(), ClienteSttpProduzione), luoghiSvizzera, db)
  }
}

/**
 * Scarica nel database le previsioni meteorologiche dal servizio dato per le località richieste.
 *
 * @param servizioMeteo Servizio meteorologico per le previsioni
 * @param luoghi        Lista di luoghi di cui scaricare le previsioni
 * @param db            Database in cui salvare le previsioni
 */
private def scaricaPrevisioni(servizioMeteo: ServizioMeteo, luoghi: Iterable[Località], db: Database): Unit = {
  val previsioni = Future.traverse(luoghi) { località =>
    println(località.nome)
    Future(servizioMeteo.ottieniPrevisione(località))
  }

  Await.result(previsioni, 30.seconds).foreach { meteo =>
    meteo.stampaPrevisione(7)
    db.salvaPrevisione(meteo)
  }
}
