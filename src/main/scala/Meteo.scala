package org.daingun.meteo

import componenti.Previsione
import pianoDati.MeteoRiga

/**
 * Interfaccia per le analisi meteorologiche.
 */
trait Meteo {

  /**
   * Fuso orario del luogo della previsione meteorologica.
   * Per esempio "Europe/Rome".
   *
   * @return Fuso orario in formato testuale
   */
  def fusoOrario: String

  /**
   * Genera le previsioni meteorologiche
   *
   * @param giorni Numero di giorni da oggi in poi da considerare nelle previsioni
   * @return Previsioni meteorologiche
   */
  def estraiPrevisione(giorni: Long): Vector[Previsione]

  /**
   * Genera le previsioni meteorologiche
   *
   * @return Previsioni meteorologiche
   */
  def estraiPrevisione(): Vector[Previsione]

  /**
   * Genera le righe per l'inserimento nel database.
   *
   * @return Una collezione di MeteoRiga
   */
  def generaRighePerDB: Vector[MeteoRiga]

  /**
   * Stampa a video le previsioni meteorologiche.
   *
   * @param giorni Numero di giorni da oggi in poi da considerare nelle previsioni
   */
  def stampaPrevisione(giorni: Long): Unit =
    for riga <- testoPrevisione(giorni: Long) do
      println(riga)

  /**
   * Genera la versione testuale delle previsioni meteorologiche
   *
   * @param giorni Numero di giorni da oggi in poi da considerare nelle previsioni
   * @return Versione testuale delle previsioni meteorologiche
   */
  private def testoPrevisione(giorni: Long): Vector[String] =
    val testo = Vector.newBuilder[String]
    testo.addOne(Previsione.INTESTAZIONE_PREVISIONE)
    testo.addAll(estraiPrevisione(giorni).iterator.map(_.stampaPrevisione(fusoOrario)))
    testo.result()

}
