package org.daingun.meteo
package pianoDati

import org.sqlite.SQLiteDataSource

import javax.sql.DataSource

/**
 * Classe che contiene i metodi per creare sorgenti dati.
 */
object CreatoreSorgenteDati {
  /**
   * Crea una sorgente dati sqlite.
   *
   * @param url stringa di connessione al database sqlite.
   * @return Sorgente dati.
   */
  def creaSorgenteDatiSqlite(url: String): DataSource = {
    val ds = SQLiteDataSource()
    ds.setUrl(url)
    ds
  }
}
