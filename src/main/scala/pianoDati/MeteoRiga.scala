package org.daingun.meteo
package pianoDati

import pianoDati.MeteoTabella.Colonne.*

import java.sql.ResultSet
import java.time.Instant

/**
 * Classe dati che rappresenta le righe nel database della tabella METEO.
 *
 * @param dataInserimento    Data di inserimento della riga
 * @param dataPrevisione     Data della previsione meteo
 * @param temperatura        Temperatura in Celsius
 * @param pressione          Pressione atmosferica in hPa
 * @param velocitàVento      Velocità del vento in km/h
 * @param direzioneVento     Direzione testuale del vento
 * @param pioggiaProbabilità Probabilità di pioggia in percentuale
 * @param fusoOrario         Fuso orario del luogo
 * @param luogo              Luogo della previsione
 * @param coordinate         Coordinate del luogo
 */
case class MeteoRiga(dataInserimento: Instant,
                     dataPrevisione: Instant,
                     temperatura: Double,
                     pressione: Double,
                     velocitàVento: Double,
                     direzioneVento: String,
                     pioggiaProbabilità: Double,
                     fusoOrario: String,
                     luogo: String,
                     coordinate: String)

/**
 * Oggetto compagno della classe MeteoRiga.
 */
object MeteoRiga:
  /**
   * Crea una MeteoRiga da un ResultSet.
   *
   * @param riga riga del database da convertire
   * @return Un oggetto che rappresenta una riga a database
   */
  def apply(riga: ResultSet): MeteoRiga =
    val dataInserimento = Instant.parse(riga.getString(DATA_INSERIMENTO))
    val dataPrevisione = Instant.parse(riga.getString(DATA_PREVISIONE))
    val temperatura = riga.getDouble(TEMPERATURA)
    val pressione = riga.getDouble(PRESSIONE)
    val velocitàVento = riga.getDouble(VELOCITÀ_VENTO)
    val direzioneVento = riga.getString(DIREZIONE_VENTO)
    val pioggiaProbabilità = riga.getDouble(PIOGGIA_PROBABILITÀ)
    val fusoOrario = riga.getString(FUSO_ORARIO)
    val luogo = riga.getString(LUOGO)
    val coordinate = riga.getString(COORDINATE)
    MeteoRiga(
      dataInserimento = dataInserimento,
      dataPrevisione = dataPrevisione,
      temperatura = temperatura,
      pressione = pressione,
      velocitàVento = velocitàVento,
      direzioneVento = direzioneVento,
      pioggiaProbabilità = pioggiaProbabilità,
      fusoOrario = fusoOrario,
      luogo = luogo,
      coordinate = coordinate
    )

end MeteoRiga
