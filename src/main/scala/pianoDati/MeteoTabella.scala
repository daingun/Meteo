package org.daingun.meteo
package pianoDati

import pianoDati.MeteoTabella.Colonne.*

import java.sql.{Connection, PreparedStatement}

/**
 * Definizioni della tabella che contiene i dati meteo.
 */
class MeteoTabella extends Tabella {

  override def dichiarazioneCreaTabella: String =
    s"""CREATE TABLE IF NOT EXISTS $NomeTabella (
       |ID INTEGER PRIMARY KEY,
       |$DATA_INSERIMENTO TEXT,
       |$DATA_PREVISIONE TEXT,
       |$TEMPERATURA REAL,
       |$PRESSIONE REAL,
       |$VELOCITÀ_VENTO REAL,
       |$DIREZIONE_VENTO TEXT,
       |$PIOGGIA_PROBABILITÀ REAL,
       |$FUSO_ORARIO TEXT,
       |$LUOGO TEXT,
       |$COORDINATE TEXT);""".stripMargin

  override def NomeTabella: String = "METEO"

  /**
   * Genera la dichiarazione di inserimento delle righe in tabella.
   *
   * @param connessione Connessione al database
   * @param righe       righe da inserire in tabella
   * @return dichiarazione sql da eseguire in batch
   */
  def generaInserimento(connessione: Connection, righe: Seq[MeteoRiga]): PreparedStatement =
    val inserimento = s"INSERT INTO $NomeTabella ($DATA_INSERIMENTO, $DATA_PREVISIONE, $TEMPERATURA, $PRESSIONE, $VELOCITÀ_VENTO, $DIREZIONE_VENTO, $PIOGGIA_PROBABILITÀ, $FUSO_ORARIO, $LUOGO, $COORDINATE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
    val dichiarazione = connessione.prepareStatement(inserimento)

    for riga <- righe do
      dichiarazione.setString(1, riga.dataInserimento.toString)
      dichiarazione.setString(2, riga.dataPrevisione.toString)
      dichiarazione.setDouble(3, riga.temperatura)
      dichiarazione.setDouble(4, riga.pressione)
      dichiarazione.setDouble(5, riga.velocitàVento)
      dichiarazione.setString(6, riga.direzioneVento)
      dichiarazione.setDouble(7, riga.pioggiaProbabilità)
      dichiarazione.setString(8, riga.fusoOrario)
      dichiarazione.setString(9, riga.luogo)
      dichiarazione.setString(10, riga.coordinate)
      dichiarazione.addBatch()

    dichiarazione

}

/**
 * Oggetto compagno che contiene il nome della tabella e quelli delle colonne.
 */
object MeteoTabella:
  /**
   * Colonne della tabella.
   */
  object Colonne {
    val DATA_INSERIMENTO: String = "DATA_INSERIMENTO"
    val DATA_PREVISIONE: String = "DATA_PREVISIONE"
    val TEMPERATURA: String = "TEMPERATURA"
    val PRESSIONE: String = "PRESSIONE"
    val VELOCITÀ_VENTO: String = "VELOCITÀ_VENTO"
    val DIREZIONE_VENTO: String = "DIREZIONE_VENTO"
    val PIOGGIA_PROBABILITÀ: String = "PIOGGIA_PROBABILITÀ"
    val FUSO_ORARIO: String = "FUSO_ORARIO"
    val LUOGO: String = "LUOGO"
    val COORDINATE: String = "COORDINATE"
  }

end MeteoTabella
