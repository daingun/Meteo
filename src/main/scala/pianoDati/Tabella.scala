package org.daingun.meteo
package pianoDati

/**
 * Tratto che definisce i metodi delle tabelle.
 */
trait Tabella {
  /**
   * Nome della tabella.
   */
  def NomeTabella: String

  /**
   * Genera la dichiarazione SQL per la creazione della tabella.
   *
   * @return La dichiarazione SQL
   */
  def dichiarazioneCreaTabella: String

  /**
   * Genera la dichiarazione SQL per la stampa completa della tabella.
   *
   * @return La dichiarazione SQL
   */
  def dichiarazioneSelezionaTabella: String = {
    s"SELECT * FROM $NomeTabella"
  }
}
