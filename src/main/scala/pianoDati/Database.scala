package org.daingun.meteo
package pianoDati

import java.sql.Connection
import javax.sql.DataSource
import scala.util.Using

/**
 * Classe che contiene i metodi per operare sul database.
 *
 * @param sorgenteDati Collegamento al database.
 */
class Database(sorgenteDati: DataSource) extends AutoCloseable {
  /**
   * Tabella contenente le previsioni meteorologiche.
   */
  private val tabellaPrevisioni: MeteoTabella = MeteoTabella()

  /**
   * Elenco delle tabelle del database.
   */
  private val tabelle: Vector[Tabella] = Vector(tabellaPrevisioni)

  /**
   * Connessione al database.
   */
  private val connessione: Connection = sorgenteDati.getConnection()

  generaTabelle()

  /**
   * Salva le previsioni meteorologiche nel database.
   *
   * @param meteo Previsioni meteorologiche
   * @return Il numero di righe inserite nel database
   */
  def salvaPrevisione(meteo: Meteo): Int =
    val dati = meteo.generaRighePerDB
    Using.resource(tabellaPrevisioni.generaInserimento(connessione, dati)) {
      insert =>
        // Disattiva il commit automatico delle transazioni per aumentare
        // le prestazioni in scrittura del database.
        try
          connessione.setAutoCommit(false)
          val conto = insert.executeBatch()
          connessione.commit()
          conto.sum
        catch
          case e =>
            connessione.rollback()
            throw e
        finally
          connessione.setAutoCommit(true)
    }

  /**
   * Stampa tutte le righe di tutte le tabelle del database.
   */
  def stampaTabelle(): Unit =
    val stampa = tabelle.map(_.dichiarazioneSelezionaTabella).mkString(";\n")
    Using.Manager { use =>
      val dichiarazione = connessione.createStatement()
      val risultato = use(dichiarazione.executeQuery(stampa))
      val numeroColonne = risultato.getMetaData.getColumnCount
      while risultato.next do
        println((1 to numeroColonne).view.map(risultato.getString).mkString(", "))
    }

  /**
   * Libera le risorse associate al database.
   */
  override def close(): Unit = Option(connessione).foreach(_.close())

  /**
   * Genera le tabelle del database.
   *
   */
  private def generaTabelle(): Unit =
    val creazioni = tabelle.map(_.dichiarazioneCreaTabella).mkString(";\n ")
    Using.resource(connessione.createStatement()) { dichiarazione =>
      val _ = dichiarazione.executeUpdate(creazioni)
    }
}

