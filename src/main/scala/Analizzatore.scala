package org.daingun.meteo

import scala.collection.Map

trait Analizzatore {
  /** Leggi il testo json.
   *
   * @param testo Testo json
   */
  def leggi(testo: String): Unit

  /** Estrai una lista di stringhe.
   *
   * @param selettore Selettori della gerarchia json per estrarre i valori.
   * @return Vettore di stringhe.
   */
  def estraiListaTesto(selettore: Int | String*): Vector[String]

  /** Estrai una lista di numeri.
   *
   * @param selettore Selettori della gerarchia json per estrarre i valori.
   * @return Vettore di numeri.
   */
  def estraiListaNumeri(selettore: Int | String*): Vector[Double]

  /** Estrai una dizionario di stringhe.
   *
   * @param selettore Selettori della gerarchia json per estrarre i valori.
   * @return Dizionario di stringhe.
   */
  def estraiDizionarioTesto(selettore: Int | String*): Map[String, String]

  /** Estrai una dizionario di numeri.
   *
   * @param selettore Selettori della gerarchia json per estrarre i valori.
   * @return Dizionario di numeri.
   */
  def estraiDizionarioNumeri(selettore: Int | String*): Map[String, Double]

  /**
   * Estrai un testo.
   *
   * @param selettore Selettori della gerarchia json per estrarre i valori.
   * @return Testo singolo.
   */
  def estraiTesto(selettore: Int | String*): String

  /**
   * Estrai un numero.
   *
   * @param selettore Selettori della gerarchia json per estrarre i valori.
   * @return Numero singolo.
   */
  def estraiNumero(selettore: Int | String*): Double

  /**
   * Estrai una lista di dizionari di stringhe.
   *
   * @param selettore Selettori della gerarchia json per estrarre i valori.
   * @return Vettore di dizionari di stringhe.
   */
  def estraiListaDizionariTesto(selettore: Int | String*): Vector[Map[String, String]]

  /**
   * Estrai la lunghezza di una lista.
   *
   * @param selettore Selettori della gerarchia json per estrarre i valori.
   * @return Lunghezza della lista.
   */
  def estraiLunghezzaLista(selettore: Int | String*): Int
}

