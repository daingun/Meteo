package org.daingun.meteo

/**
 * Interfaccia per la definizione di clienti http.
 */
trait ClienteHttp {
  /**
   * Invia una richiesta GET alla uri data.
   *
   * @param uri Uri a cui inviare la richiesta.
   * @return Risposta http.
   */
  def richiestaGet(uri: String): Risposta

  /**
   * Invia una richiesta HEAD alla uri data.
   *
   * @param uri Uri a cui inviare la richiesta.
   * @return Risposta http.
   */
  def richiestaHead(uri: String): Risposta
}