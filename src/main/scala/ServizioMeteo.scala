package org.daingun.meteo

import componenti.Località

/**
 * Interfaccia per i servizi meteorologici.
 */
trait ServizioMeteo {
  /**
   * Ottieni le previsioni dal servizio meteorologico.
   *
   * @param località Località di cui si vogliono le previsioni.
   * @return Le previsioni meteorologiche.
   */
  def ottieniPrevisione(località: Località): Meteo

  /**
   * Metodo con cui testare la correttezza delle richieste al servizio meteorologico.
   *
   * @param località Località di cui si vogliono le previsioni.
   * @return Risposta del servizio.
   */
  def testConnessione(località: Località): Risposta
}