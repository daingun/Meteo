ThisBuild / version := "0.1.1"

ThisBuild / scalaVersion := "3.3.3"

lazy val root = (project in file("."))
  .settings(
    name := "Meteo",
    idePackagePrefix := Some("org.daingun.meteo"),
    libraryDependencies ++= Seq(
      "com.softwaremill.sttp.client4" %% "core" % "4.0.0-M13",
      "com.lihaoyi" %% "upickle" % "3.3.0",
      "org.xerial" % "sqlite-jdbc" % "3.45.3.0",
      "org.scalatest" %% "scalatest" % "3.2.18" % Test
    )
  )