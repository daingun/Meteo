library(DBI)
library(ggplot2)
library(dplyr)

# Converti la direzione del vento in simbolo
conv.vento <- function(x) {
    sapply(x,
           # Funzione da applicare ad ogni elemento del vettore
           switch,
           # Argomenti addizionali della funzione
           "N" = "↓", "N-NE" = "↓", "NE" = "↙", "E-NE" = "↙",
           "E" = "←", "E-SE" = "←", "SE" = "↖", "S-SE" = "↖",
           "S" = "↑", "S-SO" = "↑", "SO" = "↗", "O-SO" = "↗",
           "O" = "→", "O-NO" = "→", "NO" = "↘", "N-NO" = "↘",
           # Caso predefinito
           NA
           )
}

con <- dbConnect(RSQLite::SQLite(), "meteo.db")
dbListTables(con)
# Filtra il sabato e la domenica del fine settimana corrente
# Restituisce un data frame
dati <- dbGetQuery(con, "SELECT * FROM METEO WHERE date(DATA_PREVISIONE) IN (date('now','-1 days','weekday 6'), date('now','weekday 0'))")
## dati <- dbGetQuery(con,"select * from METEO where DATA_PREVISIONE like '2023-10-21%' OR DATA_PREVISIONE like '2023-10-22%'")

dbDisconnect(con)

tabella <- dati %>%
    mutate(DATA_INSERIMENTO=as.POSIXct(dati$DATA_INSERIMENTO,format="%Y-%m-%dT%H:%M:%SZ",tz="UTC")) %>%
    mutate(DATA_PREVISIONE=as.POSIXct(dati$DATA_PREVISIONE,format="%Y-%m-%dT%H:%M:%SZ",tz="UTC")) %>%
    mutate(vento=conv.vento(DIREZIONE_VENTO))
#format(dati$DATA_PREVISIONE, tz="Europe/Rome",usetz=TRUE)

grafico <-
    ggplot(data=tabella) +
    geom_line(aes(x=DATA_PREVISIONE, y=TEMPERATURA), color="red") +
    geom_line(aes(x=DATA_PREVISIONE, y=VELOCITÀ_VENTO)) +
    geom_text(aes(x=DATA_PREVISIONE, y=VELOCITÀ_VENTO, label=vento)) +
    geom_label(aes(x=DATA_PREVISIONE, y=-1, label=PIOGGIA_PROBABILITÀ), size=2, label.padding=unit(0.1,"lines")) +
    scale_x_datetime(date_labels="%m/%dT%H", timezone="Europe/Rome", guide=guide_axis(angle=90)) +
    facet_grid(rows=vars(as.Date(DATA_INSERIMENTO)),cols=vars(LUOGO))

library(Cairo)
ggsave("grafico.pdf", width=8, height=4, scale=2, device=cairo_pdf)

# select DATA_INSERIMENTO, count(*) from METEO group by DATA_INSERIMENTO order by 1;
